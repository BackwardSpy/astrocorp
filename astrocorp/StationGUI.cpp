#include "Game.h"
#include "StationGUI.h"
#include "Station.h"

void StationGUI::init()
{
	sf::Vector2u screen = game.getWindowSize();

	auto *p = new GUIPanel();
	p->w = 300.0f;
	p->h = 292.0f;
	p->x = screen.x - p->w - 8.0f;
	p->y = screen.y / 2.0f - p->h;
	m_controls.push_back(std::unique_ptr<GUIPanel>(p));

	auto *label = new GUILabel();
	label->x = p->x;
	label->y = p->y;
	label->w = p->w;
	label->h = 64.0f;
	label->text = "Station";
	m_controls.push_back(std::unique_ptr<GUILabel>(label));

	auto *sellButton = new GUIButton();
	sellButton->w = p->w - 24.0f;
	sellButton->h = 64.0f;
	sellButton->x = p->x + 12.0f;
	sellButton->y = label->y + label->h + 8.0f;
	sellButton->text = "Sell Ore";
	sellButton->setCallback(m_sellCallback);
	m_controls.push_back(std::unique_ptr<GUIButton>(sellButton));

	auto *buyShipButton = new GUIButton();
	buyShipButton->w = p->w - 24.0f;
	buyShipButton->h = 64.0f;
	buyShipButton->x = p->x + 12.0f;
	buyShipButton->y = sellButton->y + sellButton->h + 8.0f;
	buyShipButton->text = "Buy Miner";
	buyShipButton->setCallback(m_buyShipCallback);
	m_controls.push_back(std::unique_ptr<GUIButton>(buyShipButton));

	auto *undockButton = new GUIButton();
	undockButton->w = p->w - 24.0f;
	undockButton->h = 64.0f;
	undockButton->x = p->x + 12.0f;
	undockButton->y = buyShipButton->y + buyShipButton->h + 8.0f;
	undockButton->text = "Undock";
	undockButton->setCallback([&]{ hide(); });
	m_controls.push_back(std::unique_ptr<GUIButton>(undockButton));
}