#pragma once

#include <SFML/Graphics.hpp>
#include "StateManager.h"

class Game
{
public:
	Game();
	~Game();

	void run();
	void exit();

	StateManager states;

	sf::Vector2u getWindowSize() { return m_window.getSize(); }

private:
	void init();
	void shutdown();

	void input();
	void update();
	void render();

	bool m_running;

	sf::RenderWindow m_window;

	sf::Clock m_frameClock;
};

// Global game instance.
extern Game game;