#pragma once

#include <list>
#include <functional>
#include <SFML/Graphics.hpp>
#include "Math.h"

class ClickableObject;

class Ship
{
public:
	Ship(float speed, const std::string &sprite);
	virtual ~Ship();

	virtual void input(const sf::Event &event) { }
	virtual void update(float dt);
	virtual void draw(sf::RenderTarget &context);

	void addForce(const Vec2 &force);

	void flyTo(ClickableObject &object, std::function<void()> callback);

	Vec2 position;
	float rotation;

protected:
	void thrust(float power);
	void rotate(float targetAngle);

	void flyTo(const Vec2 &destination);

	void fireLaser(const sf::Color &colour, float life, float size, float length);

	Vec2 m_velocity;
	float m_rotVel;

	bool m_autopilot;
	Vec2 m_targetPos;
	ClickableObject *m_targetObject;
	std::function<void()> m_targetCallback;

	float m_speed;

private:
	enum Thruster { Main, Left, Right };
	sf::Sprite m_thrusterSprites[3];
	bool m_thrusterStates[3];

	sf::Sprite m_sprite, m_laserSprite, m_laserGlowSprite;

	struct Laser
	{
		Vec2 position;
		float rotation;
		sf::Color colour;
		float lifetime, maxLife;
		float size;
		float length;

		Vec2 endPoint;
	};
	std::list<Laser> m_lasers;

	Vec2 m_force;
};