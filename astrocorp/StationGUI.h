#pragma once

#include "GUI.h"

class Station;

class StationGUI : public GUI
{
public:
	void init();
	void setStation(Station *station) { m_station = station; }

	void setSellCallback(std::function<void()> callback) { m_sellCallback = callback; }
	void setBuyShipCallback(std::function<void()> callback) { m_buyShipCallback = callback; }

private:
	Station *m_station;

	std::function<void()> m_sellCallback, m_buyShipCallback;
};