#pragma once

#include "ClickableObject.h"

class Asteroid : public ClickableObject
{
public:
	Asteroid();
	~Asteroid();

	void update(float dt);

	int ore;

private:
	float m_rotSpeed;
};