#pragma once

#include "Math.h"

class Particle
{
public:
	enum Type { GlowSmall, TypeCount };

	Particle(const Vec2 &position, const Vec2 &velocity,
		const sf::Color &startColour, const sf::Color &endColour,
		float lifetime, Type type);

	Vec2 position, velocity;
	sf::Color startColour, endColour;
	float maxLife, lifetime;
	Type type;
};