#pragma once

#include <functional>
#include <SFML/Graphics.hpp>

class StateManager;

class State
{
public:
	State(bool blocksUpdate, bool blocksDraw);
	virtual ~State() { }

	virtual void input(const sf::Event &event) = 0;
	virtual void update(float dt);
	virtual void draw(sf::RenderTarget &context);

	bool blocksUpdate() { return m_blocksUpdate; }
	bool blocksDraw() { return m_blocksDraw; }

	void setManager(StateManager *manager) { m_manager = manager; }

protected:
	void enter();
	void exit(std::function<void()> callback);

	enum Stage { Entering, Normal, Exiting };
	Stage m_stage;
	float m_transitionTimer;
	const float TRANSITION_TIME = 0.5f;
	std::function<void()> m_exitFunc;
	sf::RectangleShape m_fadeRect;

	bool m_blocksUpdate, m_blocksDraw;
	StateManager *m_manager;
};