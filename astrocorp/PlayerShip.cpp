#include "Asteroid.h"
#include "PlayerShip.h"
#include "MiningShip.h"

PlayerShip::PlayerShip():
	Ship(5.5f, "sprites/ships/player"),
	m_mouseDown(false)
{

}

PlayerShip::~PlayerShip()
{

}

void PlayerShip::input(const sf::Event &event)
{
	switch (event.type)
	{
	case sf::Event::MouseButtonPressed:
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			m_mouseDown = true;
			m_mousePos = Vec2((float)event.mouseButton.x, (float)event.mouseButton.y);
		}
		break;

	case sf::Event::MouseButtonReleased:
		if (event.mouseButton.button == sf::Mouse::Left) m_mouseDown = false;
		break;

	case sf::Event::MouseMoved:
		m_mousePos = Vec2((float)event.mouseMove.x, (float)event.mouseMove.y);
		break;
	}
}

void PlayerShip::update(float dt)
{
	Ship::update(dt);

	Vec2 diff = m_mousePos - position;
	rotate(atan2(diff.y, diff.x));
	if (m_mouseDown)
	{
		m_autopilot = false;	// Automatically disengage autopilot.
		flyTo(m_mousePos);
	}
}

void PlayerShip::draw(sf::RenderTarget &context)
{
	Ship::draw(context);
}

void PlayerShip::mine(Asteroid *asteroid)
{
	for (auto miner : m_miners)
	{
		if (!miner->hasAsteroid())
		{
			miner->mine(asteroid);
			return;
		}
	}

	// If we got to this point, everyone was already mining rocks so just pick a guy at random.
	m_miners[Math::rand(m_miners.size())]->mine(asteroid);
}