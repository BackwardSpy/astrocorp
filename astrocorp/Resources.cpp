#include "Resources.h"

Resources::~Resources()
{
	for (auto res : m_resources) delete res.second;
}