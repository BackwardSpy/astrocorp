#include <sstream>
#include "Resources.h"
#include "Station.h"

Station::Station():
	ClickableObject("sprites/objects/station"),
	ore(0)
{
	auto *font = resources.get<sf::Font>("fonts/neuropol x free");
	if (font)
	{
		m_oreText.setFont(*font);
		m_oreText.setCharacterSize(16);
		m_oreText.setColor(sf::Color::White);
	}
}

Station::~Station()
{

}

void Station::update(float dt)
{
	rotation += dt * 0.1f;
}

void Station::draw(sf::RenderTarget &context)
{
	ClickableObject::draw(context);

	m_sstream << ore << "kg of ore";
	m_oreText.setString(m_sstream.str());
	m_oreText.setPosition(position - Vec2(m_oreText.getLocalBounds().width / 2.0f, 100.0f));
	context.draw(m_oreText);
	m_sstream.str("");
}