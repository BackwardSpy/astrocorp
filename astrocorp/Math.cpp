#include <cmath>
#include "Math.h"

Vec2 Vec2::zero(0.0f, 0.0f);
Vec2 Vec2::up(0.0f, -1.0f);
Vec2 Vec2::right(1.0f, 0.0f);

Vec2::Vec2(): x(0.0f), y(0.0f) { }
Vec2::Vec2(float v): x(v), y(v) { }
Vec2::Vec2(float x, float y): x(x), y(y) { }

float Vec2::lengthSq() const
{
	return x * x + y * y;
}

float Vec2::length() const
{
	return sqrtf(lengthSq());
}

void Vec2::normalise()
{
	float l = length();
	x /= l;
	y /= l;
}

Vec2 Vec2::normalised() const
{
	float l = length();
	return Vec2(x / l, y / l);
}

float Vec2::dot(const Vec2 &other)
{
	return x * other.x + y * other.y;
}

Vec2 Vec2::operator +(const Vec2 &other) const
{
	return Vec2(x + other.x, y + other.y);
}

Vec2 Vec2::operator +=(const Vec2 &other)
{
	x += other.x;
	y += other.y;
	return *this;
}

Vec2 Vec2::operator -(const Vec2 &other) const
{
	return Vec2(x - other.x, y - other.y);
}

Vec2 Vec2::operator -=(const Vec2 &other)
{
	x -= other.x;
	y -= other.y;
	return *this;
}

Vec2 Vec2::operator *(float other) const
{
	return Vec2(x * other, y * other);
}

Vec2 Vec2::operator*=(float other)
{
	x *= other;
	y *= other;
	return *this;
}

Vec2 Vec2::operator /(float other) const
{
	return Vec2(x / other, y / other);
}

Vec2 Vec2::operator -() const
{
	return Vec2(-x, -y);
}

Vec2::operator sf::Vector2f() const
{
	return sf::Vector2f(x, y);
}

float operator *(const Vec2 &a, const Vec2 &b)
{
	return a.x * b.x + a.y * b.y;
}

namespace Math
{
	Vec2 randVec()
	{
		float ang = rand(PI * 2.0f);
		return Vec2(cos(ang), sin(ang));
	}
}