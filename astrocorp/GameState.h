#pragma once

#include "State.h"
#include "ShipManager.h"
#include "PlayerShip.h"
#include "StationGUI.h"

//debug
#include "Station.h"
#include "Asteroid.h"

class GameState : public State
{
public:
	GameState();
	~GameState();

	void input(const sf::Event &event);
	void update(float dt);
	void draw(sf::RenderTarget &context);

private:
	ShipManager m_shipManager;
	PlayerShip *m_player;

	StationGUI m_stationGUI;

	Station m_testStation;
	std::list<Asteroid> m_asteroids;

	unsigned int m_money;
	sf::Text m_moneyText;
	std::stringstream m_sstream;
};