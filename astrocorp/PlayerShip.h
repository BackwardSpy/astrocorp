#pragma once

#include <vector>
#include "Ship.h"

class Asteroid;
class MiningShip;

class PlayerShip : public Ship
{
public:
	PlayerShip();
	~PlayerShip();

	void input(const sf::Event &event);
	void update(float dt);
	void draw(sf::RenderTarget & context);

	void addMiner(MiningShip *ship) { m_miners.push_back(ship); }
	void mine(Asteroid *asteroid);

private:
	bool m_mouseDown;
	Vec2 m_mousePos;

	std::vector<MiningShip *> m_miners;
};