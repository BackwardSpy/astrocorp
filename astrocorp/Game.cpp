#include "Game.h"
#include "MainMenuState.h"

Game::Game() :
	m_running(false)
{

}

Game::~Game()
{

}

void Game::run()
{
	init();

	m_frameClock.restart();
	while (m_running)
	{
		input();
		update();
		render();
	}

	shutdown();
}

void Game::exit()
{
	m_running = false;
}

void Game::init()
{
	m_window.create(sf::VideoMode(1280, 800), "Astrocorp: Celestial Object Mining", sf::Style::Close);
	m_window.setVerticalSyncEnabled(true);

	m_running = true;

	states.add(new MainMenuState());
}

void Game::shutdown()
{
	m_window.close();
}

void Game::input()
{
	sf::Event e;
	while (m_window.pollEvent(e))
	{
		switch (e.type)
		{
		case sf::Event::Closed:
			exit();
			break;
		}

		states.input(e);
	}
}

void Game::update()
{
	float dt = m_frameClock.getElapsedTime().asSeconds();
	m_frameClock.restart();

	states.update(dt);
}

void Game::render()
{
	m_window.clear();

	states.draw(m_window);

	m_window.display();
}