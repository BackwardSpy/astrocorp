#pragma once

#include <SFML/Graphics.hpp>

class GUIPanel
{
public:
	float x, y, w, h;

	GUIPanel();
	virtual ~GUIPanel() { }

	virtual void input(const sf::Event &event) { }
	virtual void update(float dt) { }
	virtual void draw(sf::RenderTarget &context);

private:
	sf::Sprite m_patches[3][3];
	int m_tileSize;
};