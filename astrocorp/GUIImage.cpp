#include "GUIImage.h"

void GUIImage::draw(sf::RenderTarget &context)
{
	w = (float)image.getTexture()->getSize().x;
	h = (float)image.getTexture()->getSize().y;
	image.setPosition(x, y);
	context.draw(image);
}