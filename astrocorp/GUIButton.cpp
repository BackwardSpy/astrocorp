#include "GUIButton.h"
#include "Resources.h"

GUIButton::GUIButton()
{
	GUILabel::GUILabel();

	m_state = Normal;

	m_beep = sf::Sound(*resources.get<sf::SoundBuffer>("audio/rollover1"));
	m_boop = sf::Sound(*resources.get<sf::SoundBuffer>("audio/click1"));
}

void GUIButton::input(const sf::Event &event)
{
	switch (event.type)
	{
	case sf::Event::MouseMoved:
		if (inBounds(event.mouseMove.x, event.mouseMove.y))
		{
			if(m_state != Hover) m_beep.play();
			m_state = Hover;
		}
		else m_state = Normal;
		break;

	case sf::Event::MouseButtonPressed:
		if (event.mouseButton.button != sf::Mouse::Left) break;
		if (inBounds(event.mouseButton.x, event.mouseButton.y))
		{
			m_boop.play();
			if(m_callback) m_callback();
		}
		else m_state = Normal;
		break;
	}
}

void GUIButton::draw(sf::RenderTarget &context)
{
	if (m_state == Hover)
	{
		float oldX = x, oldY = y, oldW = w, oldH = h;

		x -= 4.0f;
		y -= 4.0f;
		w += 8.0f;
		h += 8.0f;

		GUILabel::draw(context);

		x = oldX;
		y = oldY;
		w = oldW;
		h = oldH;
	}
	else
	{
		GUILabel::draw(context);
	}
}

bool GUIButton::inBounds(int x, int y)
{
	return	x > this->x &&
			x < this->x + w &&
			y > this->y &&
			y < this->y + h;
}