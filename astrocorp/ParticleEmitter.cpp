#include "ParticleEmitter.h"
#include "Resources.h"

ParticleEmitter::ParticleEmitter()
{
	auto *tex = resources.get<sf::Texture>("sprites/particles/glowSmall");

	if (tex)
	{
		sf::Sprite &spr = m_sprites[Particle::GlowSmall];
		spr.setTexture(*tex);
		spr.setOrigin(tex->getSize().x / 2.0f, tex->getSize().y / 2.0f);
	}
}

ParticleEmitter::~ParticleEmitter()
{

}

void ParticleEmitter::update(float dt)
{
	auto it = m_particles.begin();
	while (it != m_particles.end())
	{
		Particle &p = *it;
		p.position += p.velocity * dt;
		p.velocity *= powf(0.5f, dt);

		p.lifetime -= dt;
		if (p.lifetime <= 0.0f) it = m_particles.erase(it);
		else ++it;
	}
}

void ParticleEmitter::draw(sf::RenderTarget &context)
{
	for (auto &p : m_particles)
	{
		sf::Color colour;
		float ratio = p.lifetime / p.maxLife;

		colour.r = sf::Uint8(p.startColour.r * ratio + p.endColour.r * (1.0f - ratio));
		colour.g = sf::Uint8(p.startColour.g * ratio + p.endColour.g * (1.0f - ratio));
		colour.b = sf::Uint8(p.startColour.b * ratio + p.endColour.b * (1.0f - ratio));

		sf::Sprite &spr = m_sprites[p.type];
		spr.setColor(colour);
		spr.setPosition(p.position);

		context.draw(spr);
	}
}