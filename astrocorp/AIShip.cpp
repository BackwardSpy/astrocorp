#include "AIShip.h"


#include "Game.h"
AIShip::AIShip(const std::string &sprite) : Ship(5.0f, sprite)
{
	m_targetPos = Vec2(Math::rand((float)game.getWindowSize().x), Math::rand((float)game.getWindowSize().y));
	m_autopilot = true;
}

AIShip::~AIShip()
{

}

void AIShip::input(const sf::Event &event)
{
}

void AIShip::update(float dt)
{
	Ship::update(dt);

	if ((m_targetPos - position).lengthSq() <= 10000.0f) m_targetPos = Vec2(Math::rand((float)game.getWindowSize().x), Math::rand((float)game.getWindowSize().y));
}

void AIShip::draw(sf::RenderTarget & context)
{
	Ship::draw(context);
}