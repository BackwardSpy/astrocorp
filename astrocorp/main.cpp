#include "Resources.h"
Resources resources;

#include "Game.h"
Game game;

#ifndef NDEBUG
int main()
#else
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
int CALLBACK WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR cmdLine, int cmdShow)
#endif
{
	srand((unsigned int)time(0));
	game.run();
	return 0;
}