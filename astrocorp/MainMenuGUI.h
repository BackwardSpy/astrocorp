#pragma once

#include "GUI.h"

class MainMenuGUI : public GUI
{
public:
	void setStartCallback(std::function<void()> callback) { m_startCallback = callback; }
	void setExitCallback(std::function<void()> callback) { m_exitCallback = callback; }

	void init();

private:
	std::function<void()> m_startCallback;
	std::function<void()> m_exitCallback;
};