#include <stack>
#include "StateManager.h"
#include "State.h"

StateManager::StateManager()
{

}

StateManager::~StateManager()
{

}

void StateManager::add(State *state)
{
	m_states.push_front(std::unique_ptr<State>(state));
	m_states.front()->setManager(this);
}

void StateManager::remove(State *state)
{
	m_removalList.push_back(state);
}

void StateManager::changeTo(State *state)
{
	m_removalList.push_back(m_states.front().get());
	m_additionList.push_back(state);
}

void StateManager::input(const sf::Event &event)
{
	for (auto &state : m_states)
	{
		state->input(event);
		if (state->blocksUpdate()) break;
	}
}

void StateManager::update(float dt)
{
	for (auto state : m_removalList) m_states.remove_if([&](std::unique_ptr<State> const &ptr){ return ptr.get() == state; });
	m_removalList.clear();

	for (auto state : m_additionList) m_states.push_front(std::unique_ptr<State>(state));
	m_additionList.clear();

	for (auto &state : m_states)
	{
		state->update(dt);
		if (state->blocksUpdate()) break;
	}
}

void StateManager::draw(sf::RenderTarget &context)
{
	std::stack<const std::unique_ptr<State> *> states;

	for (auto &state : m_states)
	{
		states.push(&state);
		if (state->blocksDraw()) break;
	}

	while (states.size() > 0)
	{
		(*states.top())->draw(context);
		states.pop();
	}
}