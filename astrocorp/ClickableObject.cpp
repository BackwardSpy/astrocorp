#include "ClickableObject.h"
#include "Resources.h"

ClickableObject::ClickableObject(const std::string &sprite) :
	clickRadius(0.0f)
{
	auto *tex = resources.get<sf::Texture>(sprite);
	if (tex)
	{
		m_sprite.setTexture(*tex);
		m_sprite.setOrigin(tex->getSize().x / 2.0f, tex->getSize().y / 2.0f);
		clickRadius = 0.9f * ((tex->getSize().x + tex->getSize().y) / 4.0f);
	}
}

ClickableObject::~ClickableObject()
{

}

void ClickableObject::input(const sf::Event &event)
{
	if (event.type != sf::Event::MouseButtonPressed || event.mouseButton.button != sf::Mouse::Right) return;
	
	float distanceSq = (Vec2((float)event.mouseButton.x, (float)event.mouseButton.y) - position).lengthSq();
	if (m_callback && distanceSq <= clickRadius * clickRadius) m_callback(*this);
}

void ClickableObject::draw(sf::RenderTarget &context)
{
	m_sprite.setPosition(position);
	m_sprite.setRotation(rotation * Math::RAD2DEG);
	context.draw(m_sprite);
}